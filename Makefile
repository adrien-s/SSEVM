CC:=g++
TARGET:=ssevm
CXXFLAGS:=-s -O2
VERSION:=1.6.432

SRCFILES=$(wildcard src/*.cpp)
OBJFILES=$(SRCFILES:.cpp=.o)

.PHONY: build clean mrproper

build: $(TARGET)

clean:
	rm -f src/*.o

mrproper: clean
	rm -f bin/$(TARGET)

$(TARGET): $(OBJFILES)
	$(CC) -lsfml-graphics -lsfml-window -lsfml-system $(OBJFILES) -o bin/$(TARGET)

%.o: %.cpp
	$(CC) $(CXXFLAGS) -I./include -D 'VERSION="$(VERSION)"' -c $< -o $@
