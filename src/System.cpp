#include <iostream>

#include <ssevm/System.h>

using namespace std;

namespace SSEM {

	// -------------------------
	// Constructor & destructor
	// -------------------------
	
	System::System()
	{
		m_mem = NULL;
		m_cpu = NULL;
		m_crt = NULL;
	}
	System::~System()
	{
		if (m_cpu != NULL)
			delete m_cpu;

		if (m_crt != NULL)
			delete m_crt;

		if (m_mem != NULL)
			delete m_mem;
	}

	// ------------
	// Init systems
	// ------------
	
	void System::init(int systems)
	{
		// -----------------------------------------------------
		// Function	: Initializes the modules
		// Inputs	: The id of the modules to be initialied
		// Outputs	: None
		// -----------------------------------------------------

		if (systems&memory)
		{
			if (m_mem != NULL)
				delete m_mem;
			m_mem = new Memory;
			m_mem->reset();

			if (m_cpu != NULL)
				m_cpu->setMemPtr(m_mem);
			if (m_crt != NULL)
				m_crt->setMem(m_mem->getPtr(), m_mem->getSize());
		}
		if (systems&cpu)
		{
			if (m_cpu != NULL)
				delete m_cpu;
			m_cpu = new Cpu;

			if (m_mem != NULL)
				m_cpu->setMemPtr(m_mem);
		}
		if (systems&crt)
		{
			if (m_crt != NULL)
				delete m_crt;
			m_crt = new Display;
			m_app = m_crt->getWindowPtr();

			if (m_mem != NULL)
				m_crt->setMem(m_mem->getPtr(), m_mem->getSize());
		}
	}
	bool System::isInitialized(int systems)
	{
		// ----------------------------------------------------------
		// Function	: Gets the modules state
		// Inputs	: The modules id
		// Outputs	: The current state (true:initialized, false:
		// 	not initialized)
		// ----------------------------------------------------------

		bool ok = true;

		if ((systems&memory) && (m_mem == NULL))
			ok = false;
		if ((systems&cpu) && (m_cpu == NULL))
			ok = false;
		if ((systems&crt) && (m_crt == NULL))
			ok = false;

		return ok;
	}

	// -------------------
	// Memory file loading
	// -------------------
	
	void System::loadFile(string filename)
	{
		m_mem->loadFromFile(filename);
        m_asmFilename = filename;
        m_hasAssembled = 0;
	}
	void System::loadAsm(string filename)
	{
        m_mem->loadFromAsm(filename);
        m_asmFilename = filename;
        m_hasAssembled = 1;
	}

	// --------------
	// Execution loop
	// --------------
	
	int System::exec()
	{
		// ----------------------------------------------------------------------
		// Function	: Main program's loop which updates the cpu and crt state
		// Inputs	: None
		// Outputs	: State (0:correctly ended, -1: something went wrong)
		// ----------------------------------------------------------------------
		
		if ((m_mem == NULL) || (m_crt == NULL) || (m_cpu == NULL) || (m_app == NULL))
		{
			cerr << "[sys] Unititialized module found." << endl;
			return -1;
		}

		cout << "[sys] Starting loop" << endl;
		m_cpu->setRunning();
		m_crt->refresh();

		bool pause = false;
		int step = 2;

		while (m_app->isOpen())
		{
			while (m_app->pollEvent(m_event))
			{
				if (m_event.type == sf::Event::Closed)
					m_crt->quit();
				if (m_event.type == sf::Event::KeyPressed)
				{
					switch (m_event.key.code)
					{
                        case sf::Keyboard::L:
                            if (m_hasAssembled)
                                loadAsm(m_asmFilename);
                            else
                                loadFile(m_asmFilename);
                            break;
						case sf::Keyboard::R:
							if (m_cpu->isRunning())
								m_cpu->setStop();
							m_mem->reset();
							break;
						case sf::Keyboard::P:
							pause = !pause;
							if (!pause) step = 0;
							break;
						case sf::Keyboard::S:
							step = 1;
							break;
						case sf::Keyboard::Space:
							step = 0;
							break;
						case sf::Keyboard::X:
							m_cpu->setRunning();
							break;
						case sf::Keyboard::H:
							m_cpu->setStop();
							break;
						default:
							break;
					}
				}
			}

			if (m_cpu->isRunning() && !pause && step<2 )
			{
				m_cpu->exec();
				if (step==1)
				{
					m_crt->updateInfo(m_cpu->getInfo());
					step++;
				}
			}
			if (m_app->isOpen())
				m_crt->refresh();
		}

		cout << "[sys] End of execution" << endl;
		return 0;
	}

}
