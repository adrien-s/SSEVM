#include <iostream>
#include <sstream>
#include <fstream>

#include <ssevm/Interpreter.h>
#include <ssevm/constants.h>

using namespace std;

namespace SSEM {

	// ------------------------
	// Constructor & destructor
	// ------------------------
	
	Interpreter::Interpreter()
	{
		m_nbLabels = 0;
	}
	Interpreter::~Interpreter()
	{
		if (m_nbLabels > 0)
		{
			for (int i = 0 ; i<m_nbLabels ; i++)
			{
				delete m_strTable[i];
				delete m_addressTable[i];
			}
			delete[] m_strTable;
			delete[] m_addressTable;
		}
	}

	// -----
	// Other
	// -----
	
	void Interpreter::setAsm(string filename)
	{
		// --------------------------------------------------
		// Function	: Sets the path of the asm file to be
		// 	loaded.
		// Inputs	: Path
		// Outputs	: None
		// --------------------------------------------------

		m_filename = filename;
	}
	void Interpreter::fillMem(int* memory, unsigned int size)
	{
		// -----------------------------------------------------------
		// Function	: Read the asm file, and fill the memory with
		// 	the assembled version of the program
		// Inputs	: Memory pointer and size
		// Outputs	: Program into the memory (unsing the pointer)
		// -----------------------------------------------------------

		fstream m_file;
		cout << "[par] Assembling file...";
		string mnemonic, param;
		int parameter, p2;
		int opcode;
		unsigned int addr;
		char t;

		for (int tr = 0 ; tr < 2 ; tr++)
		{
			m_file.open(m_filename.c_str(), fstream::in);
			addr = 0x000;
			
			while (!m_file.eof() && (addr<=size))
			{
				m_file >> mnemonic;
				if (!m_file.eof())
				{
					// If the mnemonic is a label (eg 'label:')
					if (mnemonic[mnemonic.length()-1]==':')
					{
						// If we are on the first pass, register the
						// label's address, else do nothing
						if (!tr)
							addLabel(mnemonic.substr(0, mnemonic.length()-1), addr);
						// Gets the following mnemonic
						m_file >> mnemonic;
					}

					opcode = 0x0;
					// Sets the good opcode number
					if (mnemonic == "ldn") opcode = OP_LDN;
					if (mnemonic == "sub") opcode = OP_SUB;
					if (mnemonic == "sto") opcode = OP_STO;
					if (mnemonic == "skn") opcode = OP_SKN;
					if (mnemonic == "jmp") opcode = OP_JMP;
					if (mnemonic == "jrp") opcode = OP_JRP;
					if (mnemonic == "hlt") opcode = OP_HLT;
					parameter = 0;

					if ((mnemonic != "skn") && (mnemonic != "hlt"))
					{
						// Gets the numeric parameter
						m_file >> param;
						if (tr==1)
						{
							if (param[0] == '[')
							{
								ostringstream o;
								int tmps;
								string lbl = param;
								lbl.erase(0, 1);
								tmps = lbl.rfind("]");
								lbl.erase(tmps, lbl.length());
								
								parameter = getLabel(lbl);
								o << parameter;
								lbl = o.str();
								param.replace(0, tmps+2, lbl);
								istringstream out;
								out.str(param);
								t = ' ';
								out >> parameter >> t >> p2;
								if (t=='+') parameter += p2;
								if (t=='-') parameter -= p2;
								if (t=='*') parameter *= p2;
								if (t=='/') parameter /= p2;
							}
							else
								parameter = convNum(param);
						}
						else
							parameter = convNum(param);
					}

					if (mnemonic == ".start")	// Sets the current address
						addr = parameter-1;
					else if (mnemonic == ".end")
					{
						addr = MEM_MAX+1;
						m_file.close();
					}
					else if (mnemonic == ".val" && tr==1)	// Writes the numeric value
						memory[addr] = parameter;
					else if (tr == 1)	// Writes the mnemonic & parameter
						memory[addr] = ((opcode<<13)&0xe000)
							+ ((parameter)&0x7f);
					addr++;
				}
			}
		}
		cout << "done" << endl;
	}

	// ----------------
	// Label management
	// ----------------
	
	void Interpreter::addLabel(string label, int address)
	{
		// ---------------------------------------------------------------
		// Function	: Add a label and its address into a 'dictionnary'
		// Inputs	: The label and its address
		// Outputs	: None
		// ---------------------------------------------------------------

		string** tmpstr = new string*[m_nbLabels+1];
		int** tmpaddr = new int*[m_nbLabels+1];

		for (int i = 0 ; i < m_nbLabels ; i++)
		{
			tmpstr[i] = m_strTable[i];
			tmpaddr[i] = m_addressTable[i];
		}
		tmpstr[m_nbLabels] = new string(label);
		tmpaddr[m_nbLabels] = new int(address);

		if (m_nbLabels)
		{
			delete[] m_strTable;
			delete[] m_addressTable;
		}
		m_strTable = tmpstr;
		m_addressTable = tmpaddr;

		m_nbLabels++;
	}
	int Interpreter::getLabel(std::string label)
	{
		// --------------------------------------------
		// Function	: Gets a label's memory address
		// Inputs	: The label's name
		// Outputs	: The label's address
		// --------------------------------------------

		bool found = 0;
		int i = 0, addrFound=-1;
		
		while (i<m_nbLabels && !found)
		{
			if (*m_strTable[i] == label)
			{
				found = true;
				addrFound = *m_addressTable[i];
			}
			i++;
		}

		return addrFound;
	}

	// ---
	// Num
	// ---
	
	int Interpreter::convNum(string num)
	{
		// --------------------------------------------------------------
		// Function	: Converts a number in a string in a real number,
		// 	wherever it's a decimal, hexadecimal or binary number
		// Inputs	: The string containing the number
		// Outputs	: The number represented
		// --------------------------------------------------------------

		int res;
		if (num[0] == '%')
		{
			int a = num.length();
			res = 0;

			for (int i = 1 ; i < a ; i++)
			{
				res <<= 1;
				if (num[i] == '1')
					res++;
			}
		}
		else if (num[0] == '$')
		{
			int a = num.length();
			res = 0;
			string hex="0123456789abcdef";

			for (int i = 1 ; i < a ; i++)
			{
				res <<= 4;
				res += hex.rfind(num[i]);
			}

		}
		else
		{
			istringstream iss;
			iss.str(num);
			iss >> res;
		}

		return res;
	}

}
