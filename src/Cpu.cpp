#include <iostream>
#include <string>
#include <sstream>

#include <ssevm/Cpu.h>
#include <ssevm/Memory.h>
#include <ssevm/constants.h>

using namespace std;

namespace SSEM {

	// ------------------------
	// Constructor & destructor
	// ------------------------

	Cpu::Cpu()
	{
	}
	Cpu::~Cpu()
	{
	}

	// ----------------------
	// Execute current opcode
	// ----------------------

	void Cpu::exec()
	{
		// ---------------------------------------------
		// Function	: Executes the memorized program
		// Inputs	: None
		// Outputs	: None
		// ---------------------------------------------

		if (!m_running)
		{
			cerr << "[cpu] ERROR  : Asked running loop on inactive state"
				<< endl;
			return;
		}
		if (!m_mem)
		{
			cerr << "[cpu] ERROR : Uninitialized memory operation forbidden"
				<< endl;
			setStop();
			return;
		}

		int opcode;
		int s;

		m_CI++;	// Increases the control instruction after loading the
			// opcode into RI (that's why programs starts at 1 and
			// jumps adds 1 to the target addresses)

		if (m_CI >= MEM_MAX)
		{
			cerr << "[cpu] ERROR : Control Instruction address '"
				<< m_CI << "' out of space" << endl;
			setStop();
			return;
		}

		m_RI = m_mem->read(m_CI);
		
		opcode = (m_RI>>13)&0x7;
		s = m_RI&0x7f;

		if (s >= MEM_MAX)
		{
			cerr << "[cpu] ERROR : Segment address '"
				<< s << "' out of space" << endl;
			setStop();
			return;
		}


		switch (opcode)
		{
			case OP_LDN:
				m_A = -m_mem->read(s);
				break;
			case OP_SUB:
				m_A -= m_mem->read(s);
				break;
			case OP_STO:
				m_mem->write(s, m_A);
				break;
			case OP_SKN:
				if (m_A < 0) m_CI++;
				break;
			case OP_JMP:
				m_CI = m_mem->read(s);
				break;
			case OP_JRP:
				m_CI += m_mem->read(s);
				break;
			case OP_HLT:
				setStop();
				break;
			default:
				cerr << "[cpu] ERROR : Unknown opcode '"
					<< opcode << "' at address " << m_CI << endl;
				setStop();
				break;
		}
	}

	// -----------
	// Read access
	// -----------
	
	bool Cpu::isRunning() const
	{
		return m_running;
	}
	string Cpu::getInfo() const
	{
		int op = ((m_RI>>13)&0x7);
		stringstream ss;

		ss << "A : " << m_A << "\nCI : ";
		if (m_CI<10) ss << "0";
		ss << m_CI << "\tRI : ";

		switch (op)
		{
			case OP_LDN:
				ss << "ldn ";
				break;
			case OP_SUB:
				ss << "sub ";
				break;
			case OP_STO:
				ss << "sto ";
				break;
			case OP_SKN:
				ss << "skn";
				break;
			case OP_JMP:
				ss << "jmp ";
				break;
			case OP_JRP:
				ss << "jrp ";
				break;
			case OP_HLT:
				ss << "hlt";
			default:
				ss << "unknown : " << ((m_RI>>13)&0x7);
				break;
		}
		if ((op != OP_SKN) && (op != OP_HLT))
			ss << (m_RI&0x1f);

		return ss.str();
	}

	// ------------
	// Write access
	// ------------
	
	void Cpu::setRunning()
	{
		cout << "[cpu] Started" << endl;
		m_running = true;
	}
	void Cpu::setStop()
	{
		cout << "[cpu] Stopped" << endl;
		m_running = false;
		m_A = 0x00000000;
		m_CI = 0x00000000;
	}
	void Cpu::setMemPtr(Memory* mem)
	{
		m_mem = mem;
	}
}
