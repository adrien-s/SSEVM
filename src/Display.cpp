#include <iostream>
#include <string>
#include <sstream>

#include <ssevm/Display.h>

using namespace std;

namespace SSEM {

	// ------------------------
	// Constructor & destructor
	// ------------------------
	
	Display::Display()
	{
		stringstream ss;

		m_tab = new unsigned int[MEM_MAX*32];
		for (unsigned int i = 0 ; i<(MEM_MAX*32) ; i++)
			m_tab[i] = LVL_MIN;

        ss << "SSEVM v" << VERSION;

		m_app.create(sf::VideoMode(WIDTH, HEIGHT+30, 32), ss.str());
		m_image.create(WIDTH, HEIGHT, sf::Color((CF_RED*LVL_CRT)/256, (CF_GRN*LVL_CRT)/256, (CF_BLU*LVL_CRT)/256));
		m_tex.create(WIDTH, HEIGHT);
		m_tex.loadFromImage(m_image);
		m_sprite.setTexture(m_tex);
		m_sprite.setPosition(0, 30);

		m_font.loadFromFile("/usr/share/fonts/TTF/DejaVuSansMono.ttf");
		m_text.setFont(m_font);
		m_text.setCharacterSize(12);
		m_text.setColor(sf::Color(255, 255, 255));
		m_text.setPosition(0, 0);
	}
	Display::~Display()
	{
		delete[] m_tab;
	}

	// --------
	// Displays
	// --------
	
	void Display::setMem(int* mem, int size)
	{
		m_mem = mem;
		m_memSize = size;
	}
	void Display::updateInfo(string infos)
	{
		m_text.setString(infos);
	}
	void Display::refresh()
	{
		// -------------------------------------------------------
		// Function	: Refreshes the screen, reading the memory
		// Inputs	: None
		// Outputs	: None
		// -------------------------------------------------------

		int oX = PX_MARG, oY = PX_MARG, x/*, oX2 = WIDTH*/;
		int word, cfColTmp;
		//oX2 /= 2;
		sf::Color cl;

		for(unsigned int i = 0 ; i < m_memSize ; i++)
		{
			/*if (i == m_memSize/2)
			{
				oX = oX2;
				oY = PX_MARG;
			}*/

			word = m_mem[i];
			x = oX;

			for (int j=0 ; j<32; j++)
			{
				if (word&0x1)
				{
					if (m_tab[(32*i)+j]>LVL_REF)
						m_tab[(32*i)+j] -= REM;
					else
						m_tab[(32*i)+j] = LVL_MAX;
				}
				else if (m_tab[(32*i)+j]>LVL_MIN)
					m_tab[(32*i)+j] -= REM;

				cfColTmp = m_tab[(32*i)+j];
				cl.r = (CF_RED*cfColTmp)/256;
				cl.g = (CF_GRN*cfColTmp)/256;
				cl.b = (CF_BLU*cfColTmp)/256;

				for (int a = 0 ; a<PX_SIZE ; a++)
					for (int b = 0 ; b<PX_SIZE ; b++)
						m_image.setPixel(x+a, oY+b, cl);

				x += PX_SIZE+PX_MARG;
				word = (word>>1);
			}
			oY += PX_SIZE+PX_MARG;
		}

		m_app.clear(sf::Color((CF_RED*LVL_CRT)/256, (CF_GRN*LVL_CRT)/256, (CF_BLU*LVL_CRT)/256));
		m_tex.update(m_image);
		m_app.draw(m_text);
		m_app.draw(m_sprite);
		m_app.display();
	}
	void Display::quit()
	{
		m_app.close();
	}
	sf::RenderWindow* Display::getWindowPtr()
	{
		return &m_app;
	}

}
