#include <iostream>
#include <fstream>
#include <string>

#include <ssevm/Memory.h>
#include <ssevm/Interpreter.h>
#include <ssevm/constants.h>

using namespace std;

namespace SSEM {

	// ------------------------
	// Constructor & destructor
	// ------------------------
	
	Memory::Memory()
	{
		m_memSize = MEM_MAX;
	}
	Memory::~Memory()
	{
	}

	// --------------
	// Memory loading
	// --------------
	
	void Memory::reset()
	{
		// ------------------------------------------------------
		// Function	: Resets the memory (fill it with zeroes)
		// Inputs	: None
		// Outputs	: None
		// ------------------------------------------------------

		if (m_memTab == NULL)
		{
			cerr << "[mem] ERROR : Uninitialized segment reset forbidden"
				<< endl;
			return;
		}
		cout << "[mem] Memory reset" << endl;
		for (unsigned int i = 0 ; i < m_memSize ; i++)
			m_memTab[i] = 0x00000000;
	}
	void Memory::loadFromFile(string filename)
	{
		// -------------------------------------------------
		// Function	: Load a binary file into the memory
		// Inputs	: Path to the file to be loaded
		// Outputs	: None
		// -------------------------------------------------

		fstream tmpFile;
		tmpFile.open(filename.c_str(), fstream::in|fstream::binary);
		char tmp[4];
		int i = 0;

		tmpFile.seekg(0, ios::end);
		int len = tmpFile.tellg();
		tmpFile.seekg(0, ios::beg);
		len /= 4;

		while (i<len)
		{
			tmpFile.read(tmp, 4);
			m_memTab[i] = ((tmp[0]<<24)&0xff000000) + ((tmp[1]<<16)&0xff0000) +
			((tmp[2]<<8)&0xff00) + (tmp[3]&0xff);

			i++;
		}
		tmpFile.close();
	}
	void Memory::loadFromAsm(string filename)
	{
		// ----------------------------------------------------
		// Function	: Load an assembly file into the memory
		// Inputs	: Path to the file to be loaded
		// Outputs	: None
		// ----------------------------------------------------

		Interpreter asmToBin;
		asmToBin.setAsm(filename);
		asmToBin.fillMem(m_memTab, m_memSize);
	}
	
	// -------------
	// Memory access
	// -------------
	
	int* Memory::getPtr()
	{
		return m_memTab;
	}
	unsigned int Memory::getSize() const
	{
		return m_memSize;
	}
	int Memory::read(unsigned int address) const
	{
		// ------------------------------------------------------
		// Function	: Provides a read access to a memory word
		// Inputs	: The address to be read
		// Outputs	: The address content
		// ------------------------------------------------------

		if (address > MEM_MAX)
		{
			cerr << "[mem] ERROR : Reading memory word at address '"
				<< address << "' out of space" << endl;
			return -1;
		}
		return m_memTab[address];
	}
	void Memory::write(unsigned int address, const int & var)
	{
		// -------------------------------------------------------
		// Function	: Provides a write access to a memory word
		// Inputs	: The address to be written
		// Outputs	: None
		// -------------------------------------------------------

		if (address > MEM_MAX)
		{
			cerr << "[mem] ERROR : Writing memory word at address '"
				<< address << "' out of space" << endl;
			return;
		}
		m_memTab[address] = var;
	}

}
