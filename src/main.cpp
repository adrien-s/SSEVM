/*	Project name	: Small Scale Experimental Virtual Machine
 *	Main Programmer	: Adrien Sohier
 *	Libraries used	: SFML version 2
 *
 * 	Copyright © 2012-2014, ArtSoftWare
 *
 * 	**** English ****
 *	This project is licenced under the GNU General Public License version 3.
 *	Please read the COPYING file for more information.
 *
 *	This software is provided AS-IS, in the hope that it will be usefull,
 *	but WITHOUT ANY WARRANTY. If you experienced some bugs, you can e-mail me
 *	at support@art-software.fr
 *	
 *	**** Français ****
 *	Ce projet est sous license GNU General Public License version 3.
 *	Lisez le fichier COPYING pour plus d'informations (en anglais).
 *
 *	Ce logiciel est fourni TEL QUEL, dans l'espoir qu'il puisse être utile, mais
 *	SANS GARANTIE D'AUCUNE SORTE. Malgré tout, si vous y trouvez un bogue, vous pouvez m'en informer en m'écrivant
 *	à mon adresse mail : support@art-software.fr
 */

#include <iostream>
#include <string>

#include <ssevm/constants.h>
#include <ssevm/System.h>

using namespace std;

int main(int argc, char** argv)
{
	// ---------------------------------------------------------------
	// Function	: Initializes the main objects and run the machine
	// Inputs	: Program's arguments
	// Outputs	: None.
	// ---------------------------------------------------------------

	// Welcome message
    cout << "SSEVM v" << VERSION << endl;
	string file;
	string type;

	if (argc < 3)
	{
		cout << "Parameters :" << endl << "-a <asm filename> : load memory image from asm language file" << endl
			<< "-b <bin filename> : load memory from binary image" << endl;

		return 1;
	}
	else
	{
		// Get file name and its type (assembly or binary)
		file = argv[2];
		type = argv[1];
		cout << "[sys] Looking for file '" << file << "'" << endl;
	}

	// Creating the machine
	SSEM::System vm;

	// Creates the objects (memory, cpu and crt)
	vm.init(SSEM::System::memory|SSEM::System::cpu|SSEM::System::crt);

	// Loads the memory file
	if (type == "-b")
		vm.loadFile(file);
	else if (type == "-a")
		vm.loadAsm(file);
	else
	{
		cout << "[sys] ERROR : Unknown switch '" << type << "'" << endl;
		return 1;
	}

	// If everything went good, starts the execution loop
	if (vm.isInitialized(SSEM::System::memory|SSEM::System::cpu|SSEM::System::crt))
		return vm.exec();
	else
	{
		// If something went wrong, tells to the user the machine cannot run.
		cerr << "[sys] ERROR : System cannot initialize modules." << endl;
		return -1;
	}
}
