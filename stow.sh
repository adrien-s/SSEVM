#!/usr/bin/env bash
ver=$(git describe)
ver=1.0

[ -d ssevm-${ver} ] && rm -r ssevm-${ver}

install -Dm755 bin/ssevm ssevm-${ver}/usr/bin/ssevm
install -dm755 ssevm-${ver}/usr/share/

cp -r share/SSEVM ssevm-${ver}/usr/share/
cp -r doc ssevm-${ver}/usr/share/SSEVM/

tar -cf - ssevm-${ver}|xz -ze9c > ssevm-${ver}.stow.tar.xz
rm -r ssevm-${ver}
