#ifndef class_Cpu
#define class_Cpu

#include <string>

namespace SSEM {
	class Memory;
	class Cpu {
	public:
		Cpu();
		~Cpu();
		
		void exec();

		void setRunning();
		void setStop();
		void setMemPtr(Memory* mem);

		std::string getInfo() const;
		
		bool isRunning() const;
	private:
		Memory* m_mem;
		int m_A;
		int m_CI;
		int m_RI;

		bool m_running;
	};
}

#endif // Cpu.h
