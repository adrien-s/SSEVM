#ifndef _CONSTANTS_
#define _CONSTANTS_

// opcodes
#define	OP_JMP	0x0
#define	OP_JRP	0x1
#define	OP_LDN	0x2
#define	OP_STO	0x3
#define	OP_SUB	0x4
#define	OP_SKN	0x6
#define	OP_HLT	0x7

//start <address>
//val <data>

//parameters
#define MEM_MAX	32	//simulates 1 crt tube of 32 words size
#define	REM	512	// 0<>65535, crt effect (better effect is about 512)

#define	LVL_CRT	24*256	// 0<->65535, background crt brightness
#define	LVL_MIN	32*256	// 0<->65535, minimal crt dots brightness
#define	LVL_REF	192*256	// 0<->65535, minimal crt dots refreshing brightness
#define	LVL_MAX	255*256	// 0<->65535, maximal crt dots brightness

#define	PX_SIZE	6
#define	PX_MARG	2
#define	WIDTH	32*(PX_SIZE+PX_MARG)+PX_MARG
#define	HEIGHT	MEM_MAX*(PX_SIZE+PX_MARG)+PX_MARG

#define	CF_RED	0.1	//crt color coefficient
#define	CF_GRN	1.0
#define	CF_BLU	0.5

#endif
