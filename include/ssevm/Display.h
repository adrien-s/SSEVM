#ifndef class_Display
#define class_Display

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <string>

#include "constants.h"

namespace SSEM {

	class Display {
	public:
		Display();
		~Display();

		void refresh();
		void quit();
		void setMem(int* mem, int size);
		void updateInfo(std::string infos);
		sf::RenderWindow* getWindowPtr();

	private:
		sf::RenderWindow m_app;
		sf::Texture m_tex;
		sf::Image m_image;
		sf::Sprite m_sprite;

		sf::Text m_text;
		sf::Font m_font;
		int* m_mem;
		unsigned int m_memSize;

		unsigned int* m_tab;
	};
}

#endif // Display.h
