#ifndef class_Memory
#define class_Memory

#include <string>

#include "constants.h"

namespace SSEM {
	class Memory {
	public:
		Memory();
		~Memory();

		void reset();
		void loadFromFile(std::string filename);
		void loadFromAsm(std::string filename);

		int read(unsigned int address) const;
		void write(unsigned int address, const int & var);
		int* getPtr();
		unsigned int getSize() const;
	private:
		int m_memTab[MEM_MAX];
		unsigned int m_memSize;
	};
}

#endif // Memory.h
