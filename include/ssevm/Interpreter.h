#ifndef class_Interpreter
#define class_Interpreter

#include <string>

namespace SSEM {
	class Interpreter {
	public:
		Interpreter();
		~Interpreter();

		void setAsm(std::string filename);
		void fillMem(int* memory, unsigned int size);
	private:
		void addLabel(std::string label, int address);
		int getLabel(std::string label);
		int convNum(std::string num);

		std::string m_filename;
		
		std::string** m_strTable;
		int** m_addressTable;
		int m_nbLabels;
	};
}

#endif // Interpreter.h
