#ifndef class_System
#define class_System

#include "Cpu.h"
#include "Memory.h"
#include "Interpreter.h"
#include "Display.h"

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <string>

namespace SSEM {
	class System {
	public:
		System();
		~System();

		void init(int systems);

		void loadFile(std::string filename);
		void loadAsm(std::string filename);

		bool isInitialized(int systems);

		int exec();

		// systems to be initialized
		static const int memory=0x01;
		static const int cpu=0x02;
		static const int crt=0x04;
	private:
		Memory * m_mem;
		Cpu * m_cpu;
		Display * m_crt;
        bool m_hasAssembled;
        std::string m_asmFilename;

		sf::RenderWindow * m_app;
		sf::Event m_event;
	};
}

#endif // System.h
