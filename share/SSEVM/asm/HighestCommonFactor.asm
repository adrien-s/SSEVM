.start	0
jploop:	.val	0
	
	ldn	[A]
	sto	[mA]
	ldn	[B]
	sto	[B]
	ldn	[B]
	sto	[A]
	ldn	[mA]
	sub	[A]
	skn
	jrp	[jpadr]
	sub	[B]
	sto	[B]
	sub	[const]
	skn
	jmp	[jploop]
	hlt

jpadr:	.val	-3
const:	.val	2
mA:	.val	0
A:	.val	314159265
B:	.val	217281828

.end
