.start	1
chg1:	ldn	[ln1]
	skn
	sub	[const]
	sto	[tmp]
	ldn	[tmp]
	sub	[tmp]
chg2:	sto	[ln1]

	ldn	[chg1]
	sto	[tmp]
	ldn	[tmp]
	sub	[const2]
	skn
	ldn	[const1]
	sto	[chg1]
	sub	[const3]
	sto	[chg2]

	jmp	[chg1]-1

tmp:	.val	0
const1:	.val	1207943145
const2:	.val	-134217729
const3:	.val	-8192
const:	.val	1

ln1:	.val	107464223
	.val	107791923
	.val	107792691
	.val	107464991
	.val	113762207
	.val	25616283
	.val	26007987
	.val	26007987
	.val	25680287
.end
