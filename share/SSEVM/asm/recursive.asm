.start	1
adr:	ldn	[test]
	sub	[const]
	sto	[tmp]
	ldn	[tmp]

adr2:	sto	[test]
	ldn	[adr]
	sub	[const]
	sto	[tmp]
	ldn	[tmp]
	sto	[adr]

	ldn	[adr2]
	sub	[const]
	sto	[tmp]
	ldn	[tmp]
	sto	[adr2]

	jmp	0
const:	.val	1
tmp:	.val	0
test:	.val	0
.end
