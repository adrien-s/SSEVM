.start	1
loop:	ldn	[X]
	skn
	hlt
	
	sto	[TMP]
	ldn	[TMP]
	
	ldn	[S]
	sub	[X]
	sto	[TMP]
	ldn	[TMP]
	sto	[S]
	
	ldn	[X]
	sto	[TMP]
	ldn	[TMP]
	sub	[DEC]
	sto	[X]
	
	jmp	[i0]

X:	.val	20
TMP:	.val	0
DEC:	.val	1
i0:	.val	[loop]-1
S:	.val	0
.end
