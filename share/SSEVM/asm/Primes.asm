.start	0
	jmp	[i24]
loop:	ldn	[i21]
	sto	[i21]
	ldn	[i21]
	sub	[i15]
	sto	[i21]
	ldn	[i15]
	sto	[i22]
	ldn	[i22]
	sto	[i22]
	ldn	[i22]
	sub	[i15]
	sto	[i22]
	sub	[i21]
	skn
i15:	.val	-1
	ldn	[i21]
	sto	[i23]
	ldn	[i23]
	sub	[i22]
i20:	jmp	[loop]-1
i21:	.val	1
i22:	.val	7
i23:	skn
i24:	jrp	[loop]-1
	sto	[i23]
	ldn	[i23]
	sub	[i22]
	skn
	jmp	[i20]
.end
