.start	1
loop:	ldn	[X]
	sto	[TMP]
	ldn	[TMP]
	sub	[DEC]
	sto	[X]
	skn
	jmp	[i0]
	hlt

X:	.val	16
TMP:	.val	0
DEC:	.val	1
i0:	.val	[loop]
.end
