.start	1
	ldn	[Xlo]
	sto	[mTrDiv]
	ldn	[mTrDiv]
	sto	[ans]

loop:	ldn	[mX]
	sub	[ans]
	skn
	jrp	[decloop]
	sub	[mTrDiv]
	sto	[rem]
	ldn	[rem]
	skn
	hlt

	ldn	[mTrDiv]
	sub	[const]
	sto	[ans]
	ldn	[ans]
	sto	[mTrDiv]
	jmp	[aLoop]

decloop:	.val	-3
const:	.val	1
aLoop:	.val	[loop]-1

mX:	.val	-256
Xlo:	.val	255

rem:	.val	0
mTrDiv:	.val	0

.start	31
ans:	.val	0
.end
